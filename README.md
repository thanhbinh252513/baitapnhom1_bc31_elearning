# Web học online và quản lý khóa học, quản lý người dùng

Dự án này là bài tập cuối khóa từ trung tâm tin học cybersoft.
Dự án được xây dựng từ React library.

### Tham khảo

Dự án có tham khảo luồng và giao diện của trang [Edumall](https://edumall.vn/).

### Mô tả dự án

Khách hàng có thể:

- Đăng kí , đăng nhập
- Tìm kiếm khóa học
- Hiển thị danh sách khóa học.
- Thêm, xóa khóa học vào giỏ hàng.
- Hiển thị danh mục + khóa học theo danh mục.
- Xem chi tiết khóa học.
- Xem thông tin chi tiết user.
- Thanh toán khóa học.

Người quản trị có thể:

- Quản lý (thêm, xóa, cập nhật) thông tin khóa học
- Quản lý (thêm, xóa, cập nhật) thông tin người dùng

### Links

[Link Deloy](https://elearning-project-three.vercel.app/)

### Các công nghệ sử dụng

- ReactJS
- Tailwind CSS + Flowbite
- @Redux/Toolkit
- Gsap

## Author

- Github: [Nguyễn Thanh Bình](https://gitlab.com/thanhbinh252513/baitapnhom1_bc31_elearning) - Email: thanhbinn252513@gmail.com

- Github: [Quan Vĩ Nguyên](https://gitlab.com/thanhbinh252513/baitapnhom1_bc31_elearning.git) - Email: slotdi0103@gmail.com

- [ Trần Thị Thúy] ()- Email: tranthuytb239@gmail.com
